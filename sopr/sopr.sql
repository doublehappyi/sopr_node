CREATE DATABASE IF NOT EXISTS sopr DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
use sopr;

#用户表
CREATE TABLE IF NOT EXISTS sopr_user(
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  username VARCHAR(16) UNIQUE NOT NULL,
  password VARCHAR(16) NOT NULL,
  realname VARCHAR(16) NOT NULL,
  group_id INT(10) NOT NULL,
  state INT(1) DEFAULT 0,
  last_login_time INT(10) DEFAULT 0,
  create_time INT(10) NOT NULL
);
INSERT INTO sopr_user VALUES (null, 'admin', 'admin', '易双喜', 1, 0, 1451635482, 1451635482);
INSERT INTO sopr_user VALUES (null, 'admin1', 'admin', '易双喜1', 1, 0, 1451635483, 1451635483);
INSERT INTO sopr_user VALUES (null, 'admin2', 'admin', '易双喜2', 1, 0, 1451635484, 1451635484);
INSERT INTO sopr_user VALUES (null, 'admin3', 'admin', '易双喜3', 2, 0, 1451635485, 1451635485);
INSERT INTO sopr_user VALUES (null, 'admin4', 'admin', '易双喜4', 1, 0, 1451635486, 1451635486);
INSERT INTO sopr_user VALUES (null, 'admin5', 'admin', '易双喜5', 2, 1, 1451635486, 1451635486);

#角色表
CREATE TABLE IF NOT EXISTS sopr_group(
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name VARCHAR(16) UNIQUE NOT NULL,
  is_delete INT(1) DEFAULT 0,
  create_time INT(10) NOT NULL
);
INSERT INTO sopr_group VALUES (1, '管理员', 0, 1451635482);
INSERT INTO sopr_group VALUES (2, '普通用户', 0,1451635482);

#功能模块表
CREATE TABLE IF NOT EXISTS sopr_module(
  id INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
  mkey VARCHAR(16) UNIQUE NOT NULL,
  name VARCHAR(16) UNIQUE NOT NULL,
  url VARCHAR(255) UNIQUE NOT NULL,
  is_delete INT(1) DEFAULT 0,
  create_time INT(10) NOT NULL
);

#角色模块关联表
CREATE TABLE IF NOT EXISTS sopr_group_module(
  group_id INT(10) NOT NULL,
  module_id INT(10) NOT NULL,
  PRIMARY KEY (group_id, module_id)
);

#日志表