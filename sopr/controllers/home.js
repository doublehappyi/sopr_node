/**
 * Created by db on 16/1/1.
 */
var router = require('express').Router();
var mid_user = require('../middlewares').user;
router.get('/',mid_user.check_login, function(req, res, next){
    res.render('home.html');
});

router.get('/login.html', function(req, res, next){
    res.render('login.html');
});

module.exports = router;