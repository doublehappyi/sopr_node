/**
 * Created by db on 16/1/1.
 */
var router = require('express').Router();
var admin_model = require('../models').admin;
var utils = require('../common').utils;
router.get('/login', function (req, res, next) {
    res.send('login page');
});

router.post('/login', function (req, res, next) {

});

router.get('/user/list', function (req, res, next) {
    admin_model.query_user_list(function (err, rows, fields) {
        console.log(rows);
        for(var i = 0; i < rows.length; i++){

            rows[i].create_time = utils.get_formatted_date(rows[i].create_time*1000);
            rows[i].last_login_time = rows[i].last_login_time == 0 ? '--' : utils.get_formatted_date(rows[i].last_login_time*1000);
        }
        res.render('admin/user-list.html', {code: 1, msg: "ok", count: rows.length, rows: rows});
    });
});

router.post('/user', function (req, res, next) {
    var username = req.body.username,
        password = req.body.password,
        realname = req.body.realname,
        group_id = req.body.group_id;

    admin_model.user_add(username, password, realname, group_id, function (err, rows, fields) {
        console.log(rows);
        res.redirect('/admin/user/list');
    });
});

router.put('/user/:id/state', function(req, res, next){
    var id = parseInt(req.params.id),
        state = parseInt(req.body.state);
    admin_model.change_state(id, state,function(err, rows, fields){
        res.json({code:1, msg:'ok', rows:rows});
    });
});

module.exports = router;