/**
 * Created by db on 16/1/1.
 */
var router = require('express').Router();
var model_user = require('../models').user;
var utils = require('../common').utils;
var mid_user = require('../middlewares').user;
var session = require("express-session");
router.get('/login', function (req, res, next) {
    console.log('req.cookies: ',req.cookies);
    res.render('user/login.html', {code: 1, msg: ''});
});

router.post('/login', function (req, res, next) {
    var username = req.body.username,
        password = req.body.password;

    var data = {username: username, password: password};
    model_user.query(data, function (err, rows, fields) {
        if (rows.length === 1) {
            //req.session.user = username;
            //session({cookie:{maxAge:1000*10}});
            res.cookie('is_login', username);
            //res.signedCookie('username', 'secret_str');
            res.json({code: 1, msg: '登录成功'});
        } else {
            res.json({code: 0, msg: '用户名或密码错误'});
        }
    });
});

router.get('/', mid_user.check_login, function (req, res, next) {
    model_user.query_list(function (err, rows, fields) {
        console.log(rows);
        for (var i = 0; i < rows.length; i++) {

            rows[i].create_time = utils.get_formatted_date(rows[i].create_time * 1000);
            rows[i].last_login_time = rows[i].last_login_time == 0 ? '--' : utils.get_formatted_date(rows[i].last_login_time * 1000);
        }
        res.render('user/index.html', {code: 1, msg: "ok", count: rows.length, rows: rows});
    });
});

router.post('/', function (req, res, next) {
    var data = {
        username: req.body.username,
        password: req.body.password,
        realname: req.body.realname,
        group_id: req.body.group_id
    };
    model_user.insert(data, function (err, rows, fields) {
        console.log(rows);
        res.redirect('/user');
    });
});

router.put('/:id', function (req, res, next) {
    console.log();
    console.log('req.id: ', req.id);
    var id = parseInt(req.params.id);
    console.log('id: ', id);
    var data = {
        username: req.body.username,
        password: req.body.password,
        realname: req.body.realname,
        group_id: req.body.group_id,
        state: req.body.state
    };
    console.log("data: ", data);
    model_user.update(id, data, function (err, rows, fields) {
        res.json({code: 1, msg: 'ok', rows: rows});
    });
});

module.exports = router;