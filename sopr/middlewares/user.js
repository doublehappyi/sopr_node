/**
 * Created by yishuangxi on 2016/1/3.
 */

function check_login(req, res, next){



    var originalUrl = req.originalUrl;
    console.log('originalUrl', originalUrl);
    if(originalUrl !== '/user/login' && !req.cookies.is_login){
        res.redirect('/user/login?originalUrl='+originalUrl);
    }else{
        next();
    }

    //加密／解密
    var crypto = require('crypto');

    function encrypt(str,secret) {
        var cipher = crypto.createCipher('aes192', secret);
        var enc = cipher.update(str,'utf8','hex');
        enc += cipher.final('hex');
        return enc;
    }
    function decrypt(str,secret) {
        var decipher = crypto.createDecipher('aes192', secret);
        var dec = decipher.update(str,'hex','utf8');
        dec += decipher.final('utf8');
        return dec;
    }
}
module.exports = {
    check_login:check_login
}