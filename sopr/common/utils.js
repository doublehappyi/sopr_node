/**
 * Created by db on 16/1/2.
 */
module.exports = {
    get_formatted_date: function (time) {
        var d = new Date(time);
        var year = d.getFullYear();
        var month = d.getMonth() + 1,
            month = month < 10 ? ('0' + month) : month;
        var date = d.getDate(),
            date = date < 10 ? ('0' + date) : date;
        var hour = d.getHours(),
            hour = hour < 10 ? ('0' + hour) : hour;
        var minutes = d.getMinutes(),
            minutes = minutes < 10 ? ('0' + minutes) : minutes;
        var seconds = d.getSeconds(),
            seconds = seconds < 10 ? ('0' + seconds) : seconds;

        return year + '-' + month + '-' + date + ' ' + hour + ':' + minutes + ':' + seconds;

    }
}