/**
 * Created by db on 16/1/1.
 */
var db_conf = require('../conf').db_conf;
var mysql = require('mysql');
var pool = mysql.createPool({
    connectionLimit: 10,
    host: db_conf.host,
    user: db_conf.user,
    password: db_conf.password,
    database: db_conf.database
});

module.exports = {
    pool: pool,
    query:function(query_str, callback){
        console.log('query_str: ',query_str);
        pool.getConnection(function (err, conn) {
            if (err) {
                console.log('conn err: ',err);
                callback(err); return;
            }
            conn.query(query_str, function (err, rows, fields) {
                if (err) {
                    console.log('query err: ',err);
                    callback(err);
                    return;
                }
                callback(err, rows, fields);
                conn.release();
            });
        });
    }
}