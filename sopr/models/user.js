/**
 * Created by db on 16/1/2.
 */
var mysql = require('mysql');
var db = require('../common').db;
var swig = require('swig');
var table = require('../conf').db_table;

function query_list(callback){
    var query_str = "select id, username, realname, group_id, " +
        "(select name from {{group}} where id={{user}}.group_id ) as group_name, " +
        "state, create_time, last_login_time " +
        "from {{user}} order by create_time desc";

    query_str = swig.compile(query_str)({user:table.user, group:table.group});
    //query_str = mysql.format(query_str);
    db.query(query_str, callback);
}

function insert(data, callback){
    var query_str = "insert into {{user}} values (null, ?, ?, ?, ?, ?, ?, ?)";
    var state = 1, last_login_time = 0, create_time=parseInt(new Date().getTime()/1000);
    query_str = swig.compile(query_str)({user:table.user});
    query_str = mysql.format(query_str, [data.username, data.password, data.realname, data.group_id, state, last_login_time, create_time]);
    db.query(query_str, callback);
}

function update(id, data, callback){
    var query_str = 'update {{user}} as t set ';
    var set_str_arr = [];
    var values = [];
    for (var field in data){
        if(data[field] !== undefined && data.hasOwnProperty(field)){
            set_str_arr.push('t.'+field + '=? ');
            values.push(data[field]);
        }
    }
    query_str += set_str_arr.join(',') + ' where id=? ';
    values.push(id);
    query_str = swig.compile(query_str)({user:table.user});
    query_str = mysql.format(query_str, values);
    db.query(query_str, callback);
}

function query(data, callback){
    var query_str = "select id, username, realname from {{user}} where username=? and password=?";
    query_str = swig.compile(query_str)({user:table.user});
    query_str = mysql.format(query_str, [data.username, data.password]);
    db.query(query_str, callback);
}

module.exports = {
    query_list:query_list,
    insert:insert,
    update:update,
    query:query
}