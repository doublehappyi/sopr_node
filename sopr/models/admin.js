/**
 * Created by db on 16/1/2.
 */
var mysql = require('mysql');
var db = require('../common').db;
var swig = require('swig');
var table = require('../conf').db_table;

function query_user_list(callback){
    var query_str = "select id, username, realname, " +
        "(select name from {{group}} where id={{user}}.group_id ) as group_name, " +
        "state, create_time, last_login_time " +
        "from {{user}} order by create_time desc";

    query_str = swig.compile(query_str)({user:table.user, group:table.group});
    //query_str = mysql.format(query_str);
    db.query(query_str, callback);
}

function user_add(username, password, realname, group_id, callback){
    var state = 1, last_login_time = 0, create_time=parseInt(new Date().getTime()/1000);
    var query_str = "insert into {{user}} values (null, ?, ?, ?, ?, ?, ?, ?)";
    query_str = swig.compile(query_str)({user:table.user});
    query_str = mysql.format(query_str, [username, password, realname, group_id, state, last_login_time, create_time]);
    console.log('query_str', query_str);
    db.query(query_str, callback);
}

function state_change(id, state, callback){
    var query_str = 'update {{user}} set state=? where id=?';
    query_str = swig.compile(query_str)({user:table.user});
    query_str = mysql.format(query_str, [state, id]);
    db.query(query_str, callback);
}

module.exports = {
    query_user_list:query_user_list,
    user_add:user_add,
    state_change:state_change
}