/**
 * Created by db on 16/1/1.
 */
var db_conf = {
    host: 'localhost',
    user: 'root',
    password: '11111111',
    database: 'sopr',
    charset: 'utf8'
};

var db_table = {
    'user':'sopr_user',
    'group':'sopr_group',
    'module':'sopr_module',
    'group_module':'sopr_group_module'
};

module.exports = {
    db_conf:db_conf,
    db_table:db_table
};